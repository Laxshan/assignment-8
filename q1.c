#include<stdio.h>


struct student {
	char fname[30];
	char subject[30];
	int marks;
};

int main(){
 
	struct student s[5];	

	for(int i=0;i<5;i++){
		
		printf("Enter the first name of the student: ");
		scanf("%s",&s[i].fname);
		printf("Enter the subject: ");
		scanf("%s",&s[i].subject);
		printf("Marks: ");
		scanf("%d",&s[i].marks);
		printf("\n");
	}
	
	printf("\n STUDENT DETAILS \n");
	printf("\n");

	for(int i=0;i<5;i++){
		
		printf("Name: %s \n",s[i].fname);
		printf("Subject: %s \n",s[i].subject);
		printf("Marks: %d \n",s[i].marks);
		printf("\n");
	}

}